import logging
import logging.config
import zmq
import json
import binascii
import sys 

logging.config.fileConfig('logging.ini')

logger = logging.getLogger(__name__)
 

if __name__ == '__main__':
  

    socket = zmq.Context().socket(zmq.REQ)
    
    logger.info('connecting' )
    socket.connect("tcp://127.0.0.1:5555")
    
    text = 'ssss'
    logger.info('sending:%s',text)
    socket.send_string(text)

    logger.info('recving')
    message = socket.recv()
    logger.info('recved:%s' , message)

    socket.close()