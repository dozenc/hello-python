import logging
import logging.config
import asyncio
import zmq
import zmq.asyncio

logging.config.fileConfig('logging.ini')


logger = logging.getLogger(__name__)

url = "tcp://0.0.0.0:5555"
ctx = zmq.asyncio.Context()

        
@asyncio.coroutine
def serv():
    sock = ctx.socket(zmq.REP)
    sock.bind(url)

    logger.info('ZeroMQ serving: %s' , url)

    while True:
        try:
            text = yield from sock.recv() # waits for msg to be ready
            logger.info('recv: %s' , text)

            reply = text.decode() + '>>reply'
            logger.info('send: %s' , reply)
            yield from sock.send_string(reply)

        except Exception as e:

            logger.error(e)

if __name__ == '__main__':

    loop = asyncio.get_event_loop()
    loop.run_until_complete( serv() )
    loop.close()
